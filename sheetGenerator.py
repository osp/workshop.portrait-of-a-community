import svgwrite



def draw_corner (width, height, direction=1):
    d = []
    
    d.append('l{0},{1}'.format(direction * width, height))
    d.append('l{0},{1}'.format(direction * width, -1 * height))
    d.append('l{0},{1}'.format(-1 * direction * width, -1 * height))
    d.append('l{0},{1}'.format(-1 * direction * width, height))
    d.append('m{0},{1}'.format(direction * width * 2, 0))
    
    return ' '.join(d)

sheet = svgwrite.Drawing('sheet.svg', size=('297mm','420mm'))
sheet.viewbox(0,0,297,420)

group_labels = sheet.g()
group_lines = sheet.g()


x = 1
for name in ['a', 'b', 'c']:
    tspan = sheet.tspan(name)
    text = sheet.text('', insert=(x*10,10), style='text-anchor:middle;font-family:OSP-DIN')
    text.add(tspan)
    group_labels.add(text)
    x+=1


labelWidth = 80
labelHeight = 40
corner_width = 10
corner_height = 10

labels = 4

endWidth = 30

left = []
right = []

right.append('m{0},{1}'.format(0, 0))
left.append('m{0},{1}'.format(labels * labelWidth, labelHeight))

for row in range(0,10):
    direction_right = -1 if (row % 2) else 1
    direction_left = direction_right * -1
    
    right.append('l{0},{1}'.format(direction_right * (labelWidth - corner_width), 0))
    right.append(draw_corner(corner_width, corner_height, direction_right));
    
    left.append('l{0},{1}'.format(direction_left * (labelWidth - corner_width), 0))
    left.append(draw_corner(corner_width, corner_height, direction_left));
    
    for label in range(1, (labels - 1)):
        right.append('l{0},{1}'.format(direction_right * (labelWidth - (corner_width * 2)), 0))
        right.append(draw_corner(corner_width, corner_height, direction_right));
        
        left.append('l{0},{1}'.format(direction_left * (labelWidth - (corner_width * 2)), 0))
        left.append(draw_corner(corner_width, corner_height, direction_left));
        
    right.append('l{0},{1}'.format(direction_right * (labelWidth - corner_width), 0))
    left.append('l{0},{1}'.format(direction_left * (labelWidth - corner_width), 0))
    
    if direction_right > 0:
        # Ending
        right.append('l{0},{1}'.format(direction_right * endWidth, 0))
        right.append('l{0},{1}'.format(0, labelHeight * 2))
        right.append('l{0},{1}'.format(-1 * direction_right * endWidth, 0))
        
        left.append('l{0},{1}'.format(direction_left * endWidth, 0))
        left.append('l{0},{1}'.format(0, labelHeight * 2))
        left.append('l{0},{1}'.format(-1 * direction_left * endWidth, 0))
    else:
        right.append('m{0},{1}'.format(endWidth * -1, labelHeight * -3))
        right.append('l{0},{1}'.format(0, labelHeight * 2))
        right.append('m{0},{1}'.format(endWidth, labelHeight * 3))
        
        left.append('m{0},{1}'.format(endWidth, labelHeight * -1))
        left.append('l{0},{1}'.format(0, labelHeight * 2))
        left.append('m{0},{1}'.format(endWidth * -1, labelHeight))
        
        
        
group_lines.add(sheet.path(' '.join(left), style='stroke-width:1;stroke:red;fill:none'))
group_lines.add(sheet.path(' '.join(right), style='stroke-width:1;stroke:red;fill:none'))

sheet.add(group_labels)
sheet.add(group_lines)

sheet.save()