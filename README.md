Portrait of a Community
=======================

This year the annual Libre Graphics Meeting (LGM) will be held in
Toronto. The Libre Graphics Meeting is a conference for the creators and
users of Free and Open Source graphics software. As this year will be
LGM's 10th edition we feel the need to reflect upon the LGM and the
projects that take part in it. Opensource projects communities
communicate mainly through digital channels, these channels are publicly
accessible and archived. We propose to draw portraits of these
communities by investigating their public archives.

After an introduction in the various ways OSP has visualised
(programming) projects & archives up till now, and a first impression
into the different projects and 'channels', we has tought we will
use various analysis and visualisation tactics. Sources that can be used
are amongst others: commit logs (lists of messages written when code is
added, adapted or improved), release notes (retrospective texts after
significant releases), discussions (IRC channels), mailing lists,
meeting images, conference videos, etcetera. We can investigate, map and
analyse them with tools like EtherCalc, Graphviz, Pattern, NLTK &
OpenCV. As these tools themselves are Free and Open Source, this will
mean creating a portrait of the community through its own means. In particular, we were thinking to restart the [Graphics Software History Map](http://ospublish.constantvzw.org/blog/news/ce-soir-tonight-vanavond-shmn "") that we start for LGM 2009 in Montreal.

![Image Alt](http://ospublish.constantvzw.org/images/var/resizes/lgm-2009-montreal/p1080711.JPG)
![Image Alt](http://ospublish.constantvzw.org/images/var/resizes/lgm-2009-montreal/p1080694.JPG)
![Image Alt](http://ospublish.constantvzw.org/images/var/resizes/lgm-2009-montreal/p1080695.JPG)

But after first discussions, we end up with the idea that LGM is a maybe more an heteroclite salad with strong pieces inside than the meltingpot that is announced in [the 2006's first intention text](http://dneary.free.fr/lgm06/ ""), so we decided to focus as main source on all the talks and workshops of ten LGM events. 

We are now producing graphviz diagrams that can act as sepentines and confettis for the decade party!


