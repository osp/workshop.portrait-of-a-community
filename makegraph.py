# -*- coding: utf-8 -*-
#!/usr/bin/env python

import csv

CSVPATH = 'datas/csv/beyond-the-first-decade_datas_2006-2015.csv'

with open(CSVPATH, 'rb') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
    talks = []
    for row in reader:
        talks.append(row)

