# -*- coding: utf-8 -*-
#!/usr/bin/env python

import csv
CSVPATH = 'datas/csv/beyond-the-first-decade_datas_2006-2015.csv'

with open(CSVPATH, 'rb') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
    talks = []
    for row in reader:
        talks.append(row)

projects = {}

for talk in talks:
    ps = talk['project'].split(',')
    for project in ps:
        p = project.strip()
        if p in projects:
            projects[p] += 1
        else:
            projects[p] = 1

print "PROJECTS"
print

for w in sorted(projects, key=projects.get):
    print w, projects[w]

people = {}

for talk in talks:
    presenters = talk['speaker'].split(',')
    for person in presenters:
        p = person.strip()
        if p in people:
            people[p] += 1
        else:
            people[p] = 1

print "PEOPLE"
print

for w in sorted(people, key=people.get):
   print w, people[w]

